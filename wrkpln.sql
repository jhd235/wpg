-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.18 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for yii2advanced
CREATE DATABASE IF NOT EXISTS `yii2advanced` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `yii2advanced`;

-- Dumping structure for table yii2advanced.wrkpln
CREATE TABLE IF NOT EXISTS `wrkpln` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approver` varchar(50) NOT NULL DEFAULT '0',
  `spec_cyfer_name` varchar(50) NOT NULL DEFAULT '0',
  `edu_level` varchar(50) NOT NULL DEFAULT '0',
  `edu_form` varchar(50) NOT NULL DEFAULT '0',
  `disc_code_name` varchar(50) NOT NULL DEFAULT '0',
  `kaf_ind_name` varchar(50) NOT NULL,
  `edu_year` varchar(50) NOT NULL,
  `hours_lect` varchar(50) NOT NULL,
  `hrs_sem` varchar(50) NOT NULL,
  `hrs_prac` varchar(50) NOT NULL,
  `hrs_lab` varchar(50) NOT NULL,
  `hrs_self` varchar(50) NOT NULL,
  `hrs_total` varchar(50) NOT NULL,
  `ctrl_works` varchar(50) NOT NULL,
  `frm_ctrl` varchar(50) NOT NULL,
  `keywrds` varchar(50) NOT NULL,
  `executor0` varchar(50) NOT NULL,
  `executor1` varchar(50) NOT NULL,
  `resp_xctr` varchar(50) NOT NULL,
  `recenzent` varchar(50) NOT NULL,
  `disc_aims` varchar(50) NOT NULL,
  `basic_know_skills` varchar(50) NOT NULL,
  `cal_plan` varchar(50) NOT NULL,
  `met_map` varchar(50) NOT NULL,
  `lit` varchar(50) NOT NULL,
  `prog_exam` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='workplan';

-- Dumping data for table yii2advanced.wrkpln: ~1 rows (approximately)
/*!40000 ALTER TABLE `wrkpln` DISABLE KEYS */;
INSERT INTO `wrkpln` (`id`, `approver`, `spec_cyfer_name`, `edu_level`, `edu_form`, `disc_code_name`, `kaf_ind_name`, `edu_year`, `hours_lect`, `hrs_sem`, `hrs_prac`, `hrs_lab`, `hrs_self`, `hrs_total`, `ctrl_works`, `frm_ctrl`, `keywrds`, `executor0`, `executor1`, `resp_xctr`, `recenzent`, `disc_aims`, `basic_know_skills`, `cal_plan`, `met_map`, `lit`, `prog_exam`) VALUES
	(2, 'Мурзагалиев А.Ж.', '5В070900  - «Металлургия»', 'Высшее образование (бакалавриат)', 'Очная на базе среднего общего образования', 'РОРІ 3202 – Переработка и обогащение полезных иско', '35-2-3 – «Металлургия»', '2014-15', '15', '11', '15', '15', '90', '100', '2', 'Экзамен', 'руда;   концентрат;   извлечение; хвость;  извлече', 'Жүнісқалиев Т.Т.', 'Саитов Р.И.', 'Жумагалиев Е.У.', 'Мурзагалиев А.Ж.', 'изучение будущими металлургами процессов и основны', 'Студент должен:  •	иметь представление:  	о совре', 'Календарно-тематический план и сетка часов дисципл', 'Учебно-методическая карта дисциплины', 'Список основной и дополнительной литературы', 'Программа экзамена / диф. зачета');
/*!40000 ALTER TABLE `wrkpln` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

<?php 
	session_start();

	if (!isset($_SESSION['username'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}

	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['username']);
		header("location: login.php");
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Редактор Учебных Планов</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class = "loggedin">
	<div class="header">
		<h2>Редактор Учебных Планов</h2>
	</div>
	<div class="content">

		<!-- notification message -->
		<?php if (isset($_SESSION['success'])) : ?>
			<div class="error success" >
				<h3>
					<?php 
						echo $_SESSION['success']; 
						unset($_SESSION['success']);
					?>
				</h3>
			</div>
		<?php endif ?>

		<!-- logged in user information -->
		<?php  if (isset($_SESSION['username'])) : ?>
		
			<p>Вы вошли в систему <strong><?php echo $_SESSION['username']; ?></strong></p>
			<p> <a href="index.php?logout='1'" style="color: red;">Выйти</a> </p>
                        <p> <a href="ManageApprover.php" style="color: blue;">Редактировать Справочник Утверждающих</a> </p>
                        <p> <a href="docx.php" style="color: blue;">Заполнить Учебный План</a> </p>
		<?php endif ?>
		</div>
	</div>
		
</body>
</html>
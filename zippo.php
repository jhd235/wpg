<?php

class docxml {

    private $docx = 'docx.docx';
    private $zipfile = 'docx.zip';
    private $approver = 'approver';
    private $approver_real = 'default';
    private $scn = 'scn';
    private $scn_real = 'default';

    public function setprops($name, $val) {
        $this->$name = $val;
    }

    public function getprops($name) {
        print($this->$name);
    }

    public function filltemplate() {
        try {
            if (file_exists($this->docx)) {
                rename($this->docx, $this->zipfile);
            }
        }//try
        catch (Exception $e) {
            echo 'caught exception: ', $e->getMessage(), "\n";
        }
       
        $zip = new ZipArchive;
        $res = $zip->open($this->zipfile);
        if ($res === TRUE) {
            echo getcwd() . "\n";
            $zip->extractTo(getcwd() . "/extracted");
            $zip->close();
        } else {
            print("Cannot open zip.");
        }
        $docxml = getcwd() . '/extracted/word/document.xml';
        // the following line prevents the browser from parsing this as HTML.
        header('Content-Type: text/plain');
        // get the file contents, assuming the file to be readable (and exist)
        $contents = file_get_contents($docxml);
        if (stripos($contents, $this->approver) !== false) {
            $contents = str_replace($this->approver, $this->approver_real, $contents);
            $contents = str_replace($this->scn, $this->scn_real, $contents);
            file_put_contents($docxml, $contents);
            $zipo = new ZipArchive;
            $res = $zipo->open($this->zipfile);
            if ($res === TRUE) {
                $zipo->deleteName('word/document.xml');
                $zipo->addFile('extracted/word/document.xml', 'word/document.xml');
                $zipo->close();
            } else {
                echo 'failed';
            }
            rename("docx.zip", "out.docx");
            //copy("docx.zip", "out.docx");
        }//endif
    }

//function filltemplate ends;
}

// class docxml ends;

class punisher {

    public static function deleteDir($dirPath) {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

}
 copy("template.docx", "docx.docx");
$obj = new docxml();
$obj->setprops('docx', 'docx.docx');
$obj->setprops('zipfile', 'docx.zip');
$obj->setprops('approver', 'approver');
$obj->setprops('approver_real', $_POST['approver']);
$obj->setprops('scn_real', $_POST['scn']);
// $obj->getprops('docx');
$obj->filltemplate();
$punish = new punisher();
$punish->deleteDir("extracted/");
unlink("docx.zip");

header('Location: out.docx');
//unlink("out.docx");